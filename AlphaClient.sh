#!/bin/bash

DATE=`date "+%a, %d %b %Y - %H:%M:%S"`
LOG="SSH Login attemps $DATE at $HOSTNAME"
DIR=/opt/scripts/alpha-client
SERVER=192.168.155.143

ssh-logging () {
	touch $DIR/logs/AlphaClient.log
	echo $LOG >> $DIR/logs/AlphaClient.log 
	scp $DIR/logs/AlphaClient.log root@$SERVER:/opt/scripts/AlphaServer/clients/$HOSTNAME.log >/dev/null 2>&1
}

total-attempt () {
	wc -l $DIR/logs/AlphaClient.log | awk '{printf $1}' > $DIR/total-attemps.txt
	echo " ssh log-in attempts were made at $HOSTNAME" >> $DIR/total-attemps.txt
}

ssh-logging
total-attempt

echo "============================================"
echo -n " " && cat $DIR/total-attemps.txt
echo "============================================"
rm -f $DIR/total-attemps.txt
