#!/bin/bash
DIR=/opt/scripts/alpha-client

total-attempt () {
        wc -l $DIR/logs/AlphaClient.log | awk '{printf $1}' > $DIR/total-attemps.txt
        echo " ssh log-in attempts were made at $HOSTNAME" >> $DIR/total-attemps.txt
}

total-attempt

echo "============================================"
echo -n " " && cat $DIR/total-attemps.txt
echo "============================================"
rm -f $DIR/total-attemps.txt
