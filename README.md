# Alpha Client



## Getting started

This project is intended to fulfill the written test for the position of DevOps Engineer at Pajak.io.
I'm hoping to pass this test so that I can join Pajak.io team.

## Pre Requisite

1. connection SSH from client to server without password (by ssh key or etc) for user root
2. require changing the environment variable "SERVER" (line 6) on alpha-client/AlphaClient.sh file


## Execute this command on client node with root user

```
mkdir /opt/scripts
cd /opt/scripts
git clone repo 
ln -s /opt/scripts/alpha-client/AlphaClient.sh /etc/profile.d/AlphaClient.sh
ln -s /opt/scripts/alpha-client/AlphaClientCheck.sh /usr/local/bin/alphaclient
```

## To check total attempts on this client, use command 
```
alphaclient
```

Salam,
Muhammad Ridho
